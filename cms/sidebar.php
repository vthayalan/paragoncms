<?php include "includes/db.php"; 
$counter = 0;

?>


<div class="col-md-4">

    <!-- Blog Search Well -->
    <div class="well">
        <h4>Blog Search</h4>
        <div class="input-group">
            <input type="text" class="form-control">
            <span class="input-group-btn">
                <button class="btn btn-default" type="button">
                    <span class="glyphicon glyphicon-search"></span>
            </button>
            </span>
        </div>
        <!-- /.input-group -->
    </div>

    <!-- Blog Categories Well -->
    <div class="well">
        <h4>Blog Categories</h4>
        <div class="row">
            <div class="col-lg-6">
                <ul class="list-unstyled">
                    <?php


                      $query = "SELECT * FROM category";
                      $responce = mysqli_query($connect, $query);
                        while ($row = mysqli_fetch_assoc($responce)) {
				if ($counter % 2 == 0) {       
                    echo "<li><a href=\"#\">{$row['category_name']}</a></li>";
				}
                          $counter++;
                        }
                    ?>
                </ul>
            </div>
            <!-- /.col-lg-6 -->
            <div class="col-lg-6">
                <ul class="list-unstyled">
		    <?php
		      $counter = 0;
		      $res = mysqli_query($connect, $query); 
		      while ($rowtwo = mysqli_fetch_assoc($res)) {
		      if ($counter % 2 != 0) {
			      echo "<li><a href=\"#\">{$rowtwo['category_name']}</a></li>";
		      }
                      $counter++;
                      }
                    ?>
                </ul>
            </div>
            <!-- /.col-lg-6 -->
        </div>
        <!-- /.row -->
    </div>

    <!-- Side Widget Well -->
    <div class="well">
        <h4>Side Widget Well</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
    </div>

</div>

</div>
